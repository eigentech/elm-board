import Http
import Html exposing (Html, button, div, text, input)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (style, placeholder, value)
import Json.Decode exposing (Decoder, int, string, list)
import Json.Decode.Pipeline exposing (decode, required)

main =
  Html.beginnerProgram { model = model, view = view, update = update }


-- MODEL

type alias Idea =
  { id : Int
  , title : String
  , description: String
  , person: String
  , votes: Int
  }

decodeIdea : Decoder Idea
decodeIdea =
  decode Idea
    |> required "id" int
    |> required "title" string
    |> required "description" string
    |> required "person" string
    |> required "votes" int

type alias Model =
  { new: Idea
  , saved: List Idea
  }

type alias Ideas =
  { ideas: List Idea}

decodeIdeas: Decoder Ideas
decodeIdeas =
  decode
    Ideas
    |> required "ideas" (list decodeIdea)

emptyNewIdea n =
    Idea (getNewId n) "" "" "" 0

model : Model
model = initialModel


emptyModel : Model
emptyModel = Model
  (emptyNewIdea -1)
  []

initialModel : Model
initialModel = Model
  (Idea 2 "" "" "" 0)
  (sortAndReverse [ Idea 0 "Idea title" "This is a description of the idea" "Douglas" 0,
                    Idea 1 "Hackday ideas board" "Whose idea was this, it's great" "Who" 100
                  ])

-- Helpers
sortAndReverse: List Idea -> List Idea
sortAndReverse ideas =
    List.reverse (List.sortBy .votes ideas)

getIdeas: Http.Request Ideas
getIdeas =
    Http.get "http://127.0.0.1:5000/" decodeIdeas

getNewId: Int -> Int
getNewId n = if n < 0 then 0 else n + 1

onVote: Int -> Int -> Idea -> List Idea -> List Idea
onVote id vote new ideas =
      if new.id == id then
        {new | votes = new.votes + vote}::ideas
      else
        new::ideas

onDelete: Int -> Idea -> List Idea -> List Idea
onDelete id new ideas =
      if new.id == id then
        ideas
      else
        new::ideas

onAdd: Idea -> List Idea -> List Idea
onAdd new ideas =
      if String.isEmpty new.title || String.isEmpty new.person then
        ideas
      else
        sortAndReverse (ideas ++ [new])

updateField: String -> String -> String
updateField current new =
      if String.isEmpty new then
        current
      else
        new

onEdit: Int -> String -> String -> String -> Idea -> List Idea -> List Idea
onEdit id title description person new ideas =
      if new.id == id then
        {new |
          title = (updateField new.title title),
          description = (updateField new.description description),
          person = (updateField new.person person)}::ideas
      else
        new::ideas


-- UPDATE

type Msg = Add
          | Reset
          | UpdateTitle String
          | UpdateDescription String
          | UpdatePerson String
          | Vote Int Int
          | Delete Int
          | Sort
          | Edit Int String String String

update : Msg -> Model -> Model
update msg {new, saved} =
  case msg of
    Add ->
      Model
        (emptyNewIdea new.id)
        (onAdd new saved)

    Reset ->
      emptyModel

    Sort ->
      Model
        new
        (sortAndReverse saved)

    Edit id title description person ->
      Model
        new
        (List.foldr (onEdit id title description person) [] saved)

    UpdateTitle title ->
      Model
        { new | title = title }
        saved

    UpdateDescription description ->
      Model
        { new | description = description }
        saved

    UpdatePerson person ->
      Model
        { new | person = person }
        saved

    Vote id v ->
      Model
        new
        (List.foldr (onVote id v) [] saved)

    Delete id ->
      Model
        new
        (List.foldr (onDelete id) [] saved)


-- VIEW

view : Model -> Html Msg
view model =
  div [style containerStyle]
    [ viewNewIdea model.new
    , div [] (List.map makeDiv model.saved)
    , button [ onClick Reset ] [ text "reset" ]
    , button [ onClick Sort ] [ text "sort" ]
    ]

makeDiv: Idea -> Html Msg
makeDiv idea =
  div [style cardStyle]
  [ div [style titleStyle] [text (toString idea.id ++ " " ++ idea.title)]
  , div [] [text (idea.description)]
  , div [] [text (idea.person)]
  , viewIdeaStatus idea
  , div [] [text (toString idea.votes)]
  , button [ onClick (Vote idea.id 1) ] [ text "Upvote" ]
  , button [ onClick (Vote idea.id -1) ] [ text "Downvote" ]
  , button [ onClick (Delete idea.id) ] [ text "Delete" ]
  , button [ onClick (Edit idea.id "" "awesome" "cool") ] [ text "Edit" ]
  ]

viewNewIdea : Idea -> Html Msg
viewNewIdea idea =
   div []
    [ input [ placeholder "Title", value idea.title, onInput UpdateTitle ] [ text idea.title ]
    , input [ placeholder "Description", value idea.description, onInput UpdateDescription ] [ text idea.description ]
    , input [ placeholder "Person who proposed the idea", value idea.person, onInput UpdatePerson ] [ text idea.person ]
    , button [ onClick Add ] [ text "Submit" ]
    ]

viewIdeaStatus : Idea -> Html msg
viewIdeaStatus idea =
  let
    (color, status) =
      if idea.votes < 0 then
        ("red", "Nope")
      else if idea.votes < 5 then
        ("orange", "Maybe")
      else
        ("green", "Let's do this!")
  in
    div [ style [("color", color)] ] [ text status ]

containerStyle : List (String, String)
containerStyle =
  [ ("display", "flex")
  , ("flex", "1")
  , ("flexDirection", "column")
  , ("justifyContent", "center")
  , ("alignItems", "center")
  ]

cardStyle : List (String, String)
cardStyle =
  [ ("flexDirection", "row")
  , ("justifyContent", "space-between")
  , ("alignItems", "center")
  , ("padding", "10px")
  , ("margin", "5px")
  , ("border", "1px solid")
  ]

titleStyle: List (String, String)
titleStyle =
  [ ("fontSize", "30px")
  , ("fontWeight", "bold")
  , ("margin", "2px")
  , ("border", "1px dotted")
  ]
